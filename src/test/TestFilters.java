package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aaxiscommerce.acp.acrs.endeca.filters.Filter;
import com.aaxiscommerce.acp.acrs.endeca.filters.Filters;

public class TestFilters {

	@Test
	public void testSimpleFilter() {

		Filter f = Filters.OR(123l, 456l, 789l);
		assertEquals("OR(123,456,789)", f.toRecordFilter());

	}
	
	@Test
	public void testNestedFilter() {

		Filter f = Filters.OR(
			Filters.NOT(
				Filters.OR(123l, 456l, 789l)
			),
			Filters.AND(
				Filters.Property("P_Id", 123), Filters.Property("P_InStock", "T")
			)
		);
		
		assertEquals("OR(NOT(OR(123,456,789)),AND(P_Id:123,P_InStock:T))", f.toRecordFilter());
		
	}
	
	@Test
	public void testEscapedPropertyValue() {
		
		Filter f = Filters.Property("P_Name", "Product, \\(123): Name");
		
		assertEquals("P_Name:Product\\, \\\\\\(123\\)\\: Name", f.toRecordFilter());
		
	}

}
