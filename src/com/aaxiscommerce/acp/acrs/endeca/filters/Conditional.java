package com.aaxiscommerce.acp.acrs.endeca.filters;

import java.util.List;
import static com.aaxiscommerce.acp.acrs.endeca.filters.FilterUtil.join;

public class Conditional extends Filter {

	private String op;
	private List<Filter> inner;
			
	public Conditional(String op, List<Filter> inner) {
		this.op = op;
		this.inner = inner;
	}

	@Override
	public String toRecordFilter() {
		StringBuilder buf = new StringBuilder();
		buf.append(op)
		   .append("(");
		join(buf, inner);
		buf.append(")");
		return buf.toString();
	}

}
