package com.aaxiscommerce.acp.acrs.endeca.filters;

import static com.aaxiscommerce.acp.acrs.endeca.filters.FilterUtil.escapeValue;

public class PropertyValue extends Filter {

	private String name;
	private Object value;
	
	public PropertyValue(String name, Object value) {
		this.name = name;
		this.value = value;
	}

	@Override
	public String toRecordFilter() {
		return name + ":" + escapeValue(value.toString());
	}

}
