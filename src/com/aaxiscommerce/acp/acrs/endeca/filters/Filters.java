package com.aaxiscommerce.acp.acrs.endeca.filters;

import static com.aaxiscommerce.acp.acrs.endeca.filters.FilterUtil.makeDimensionValueList;

import java.util.Arrays;

public class Filters {

	private static final String AND = "AND";
	private static final String OR = "OR";
	private static final String NOT = "NOT";
	
	private Filters() {
	}
	
	public static Filter AND(Filter... criteria) {
		return new Conditional(AND, Arrays.asList(criteria));
	}
	
	public static Filter AND(long... dimValIds) {
		return new Conditional(AND, makeDimensionValueList(dimValIds));
	}

	public static Filter OR(Filter... criteria) {
		return new Conditional(OR, Arrays.asList(criteria));
	}
	
	public static Filter OR(long... dimValIds) {
		return new Conditional(OR, makeDimensionValueList(dimValIds));
	}
	
	public static Filter NOT(Filter... criteria) {
		return new Conditional(NOT, Arrays.asList(criteria));
	}
	
	public static Filter Property(String name, Object value) {
		return new PropertyValue(name, value);
	}
	
}
