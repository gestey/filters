package com.aaxiscommerce.acp.acrs.endeca.filters;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterUtil {

	private FilterUtil() {}
	
	public static List<Filter> makeDimensionValueList(long[] dimValIds) {
		Filter[] filter = new Filter[dimValIds.length];
		for (int i = 0; i < dimValIds.length; i++) {
			filter[i] = new DimensionValue(dimValIds[i]);
		}
		return Collections.unmodifiableList(Arrays.asList(filter));
	}

	public static String join(List<Filter> filters) {
		return join(new StringBuilder(), filters);
	}
	
	public static String join(StringBuilder buf, List<Filter> filters) {
		boolean first = true;
		for (Filter f: filters) {
			if (!first) {
				buf.append(",");
			}
			buf.append(f.toRecordFilter());
			first = false;
		}
		return buf.toString();
	}
	
	// TODO: this should use the real method: com.endeca.infront.navigation.util.RecordFilterUtil.escapeRecordFilterValue()
	public static String escapeValue(String value) {
		return value.replace("\\", "\\\\").replace("(", "\\(").replace(")", "\\)").replace(",", "\\,").replace(":", "\\:");
	}
	
}
