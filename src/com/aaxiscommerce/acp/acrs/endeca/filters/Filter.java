package com.aaxiscommerce.acp.acrs.endeca.filters;

public abstract class Filter {

	public Filter() {
		
	}
	
	public abstract String toRecordFilter();	

}
