package com.aaxiscommerce.acp.acrs.endeca.filters;

public class DimensionValue extends Filter {

	private long id;

	public DimensionValue(long id) {
		this.id = id;
	}
	
	@Override
	public String toRecordFilter() {
		return Long.toString(id);
	}

}
